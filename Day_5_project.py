#Password Generator Project
import random

letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

print("Welcome to the PyPassword Generator!")
nr_letters= int(input("How many letters would you like in your password?\n")) 
nr_symbols = int(input(f"How many symbols would you like?\n"))
nr_numbers = int(input(f"How many numbers would you like?\n"))

#Eazy Level - Order not randomised:
#e.g. 4 letter, 2 symbol, 2 number = JduE&!91

# n = random.randint(1,len(letters)-1)
# letter = letters[n]
# print(letter)
new_letters = []
for i in range(1,nr_letters+1):
   n = random.randint(1,len(letters)-1)
   letter = letters[n]
   # print(letter)
   new_letters.append(letter)

str_letters = ""
for i in new_letters:
   str_letters+=i
# print(str_letters)

new_symbols = []
for i in range(1,nr_symbols+1):
   n = random.randint(1,len(symbols)-1)
   symbol = symbols[n]
   # print(symbol)
   new_symbols.append(symbol)

str_symbols = ""
for i in new_symbols:
   str_symbols+=i
# print(str_symbols)

new_numbers = []
for i in range(1,nr_numbers+1):
   n = random.randint(1,len(numbers)-1)
   number = numbers[n]
   # print(number)
   new_numbers.append(number)
str_numbers = ""
for i in new_numbers:
   str_numbers+=i
# print(str_numbers)

# generate password with ordred characters !
password = str_letters+str_symbols+str_numbers
print(f"Here is your password: {password}")

#Hard Level - Order of characters randomised:
#e.g. 4 letter, 2 symbol, 2 number = g^2jk8&P
big_list = new_letters+new_symbols+new_numbers
# print(big_list)
random.shuffle(big_list)
# print(big_list)
str_big_list = ""
for i in big_list:
   str_big_list+=i
# print(str_big_list)
randomized_password = str_big_list
print(f"Here is your randomized password: {randomized_password}")
