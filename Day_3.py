# # # # # # Conditional statements, Logical Operators, Code Blocks and Scope
# # # # # # 🚨 Don't change the code below 👇
# # # # # number = int(input("Which number do you want to check? "))
# # # # # # 🚨 Don't change the code above 👆

# # # # # #Write your code below this line 👇
# # # # # if number%2==0 : 
# # # # #     print("This is an even number.")
# # # # # else : 
# # # # #     print("This is an odd number.")

# # # # # 🚨 Don't change the code below 👇
# # # # height = float(input("enter your height in m: "))
# # # # weight = float(input("enter your weight in kg: "))
# # # # # 🚨 Don't change the code above 👆

# # # # #Write your code below this line 👇
# # # # bmi = weight / (height**2)

# # # # bmi_rounded = round(bmi)
# # # # if bmi < 18.5:
# # # #    print(f"Your BMI is {bmi_rounded}, you are underweight.")
# # # # elif 18.5<bmi<25:
# # # #    print(f"Your BMI is {bmi_rounded}, you have a normal weight.")
# # # # elif 25<bmi<30:
# # # #    print(f"Your BMI is {bmi_rounded}, you are slightly overweight.")
# # # # elif 30<bmi<35:
# # # #    print(f"Your BMI is {bmi_rounded}, you are obese.")
# # # # else:
# # # #    print(f"Your BMI is {bmi_rounded}, you are clinically obese.")
# # # # # 31 day2 
# # # # 🚨 Don't change the code below 👇


# # # year = int(input("Which year do you want to check? "))
# # # # 🚨 Don't change the code above 👆

# # # #Write your code below this line 👇
# # # if year % 4==0:
# # #    if year%100==0:
# # #       if year%400==0:
# # #          print("Leap year.")
# # #       else : 
# # #          print("Not leap year.")
# # #    else : 
# # #       print("Leap year.")
# # # else : 
# # #    print("Not leap year.")

# # # 🚨 Don't change the code below 👇
# # print("Welcome to Python Pizza Deliveries!")
# # size = input("What size pizza do you want? S, M, or L ")
# # add_pepperoni = input("Do you want pepperoni? Y or N ")
# # extra_cheese = input("Do you want extra cheese? Y or N ")
# # # 🚨 Don't change the code above 👆

# # #Write your code below this line 👇
# # bill = 0
# # if size == "S":
# #    bill +=15
# #    if add_pepperoni == "Y":
# #       bill+=2
# #    if extra_cheese == "Y":
# #       bill+=1
# # elif size == "M":
# #    bill +=20
# #    if add_pepperoni == "Y":
# #       bill+=3
# #    if extra_cheese == "Y":
# #       bill+=1
# # else :
# #    bill +=25
# #    if add_pepperoni == "Y":
# #       bill+=3
# #    if extra_cheese == "Y":
# #       bill+=1
# # print(f"Your final bill is: ${bill}.")
# # 🚨 Don't change the code below 👇
# print("Welcome to the Love Calculator!")
# name1 = input("What is your name? \n")
# name2 = input("What is their name? \n")
# # 🚨 Don't change the code above 👆

# #Write your code below this line 👇

# lower_name1 = name1.lower()
# lower_name2 = name2.lower()
# both_names = f"{lower_name1}{lower_name2}"
# appearance_true = both_names.count("t")+both_names.count("r")+both_names.count("u")+both_names.count("e")
# appearance_love = both_names.count("l")+both_names.count("o")+both_names.count("v")+both_names.count("e")
# # print(appearance_true)
# # print(appearance_love)
# love_score = f"{appearance_true}{appearance_love}"
# # print(love_score)
# int_love_score=int(love_score)
# if int_love_score<10 or int_love_score>90:
#    print(f"Your score is {int_love_score}, you go together like coke and mentos.")
# elif 40<int_love_score<50:
#    print(f"Your score is {int_love_score}, you are alright together.")
# else :
#    print(f"Your score is {int_love_score}.")
print('''
*******************************************************************************
          |                   |                  |                     |
 _________|________________.=""_;=.______________|_____________________|_______
|                   |  ,-"_,=""     `"=.|                  |
|___________________|__"=._o`"-._        `"=.______________|___________________
          |                `"=._o`"=._      _`"=._                     |
 _________|_____________________:=._o "=._."_.-="'"=.__________________|_______
|                   |    __.--" , ; `"=._o." ,-"""-._ ".   |
|___________________|_._"  ,. .` ` `` ,  `"-._"-._   ". '__|___________________
          |           |o`"=._` , "` `; .". ,  "-._"-._; ;              |
 _________|___________| ;`-.o`"=._; ." ` '`."\` . "-._ /_______________|_______
|                   | |o;    `"-.o`"=._``  '` " ,__.--o;   |
|___________________|_| ;     (#) `-.o `"=.`_.--"_o.-; ;___|___________________
____/______/______/___|o;._    "      `".o|o_.--"    ;o;____/______/______/____
/______/______/______/_"=._o--._        ; | ;        ; ;/______/______/______/_
____/______/______/______/__"=._o--._   ;o|o;     _._;o;____/______/______/____
/______/______/______/______/____"=._o._; | ;_.--"o.--"_/______/______/______/_
____/______/______/______/______/_____"=.o|o_.--""___/______/______/______/____
/______/______/______/______/______/______/______/______/______/______/_____ /
*******************************************************************************
''')
print("Welcome to Treasure Island.")
print("Your mission is to find the treasure.") 
question_1 = input("left or right? ").lower()
question_2 = input("swim or wait? ").lower()
question_3 = input("wich door? ").lower()
if question_1 == "left":
   if question_2 =="wait":
      if question_3 =="blue":
         print("Game Over.")
      elif question_3=="yellow":
         print("You Win!")
      elif question_3=="red":
         print("Game Over.")
   else : 
      print("Game Over.")
else : 
   print("Game Over.")
